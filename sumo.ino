#include <AFMotor.h>
#include <SoftwareSerial.h>
#include <Servo.h>

#define PIN_LED   37
#define PIN_SERVO 39
#define PIN_MIC   A8

#define T_ENOJO   3000
#define RUIDO     80

#define ENOJADO   110
#define TRANQUI   70

Servo s;

int enojo = 0;
long antes;

// ===========================================

#define RANGO 4

#define MANUAL_X  A8
#define MANUAL_Y  A9

#define B1  40
#define B2  41
#define B3  42
#define B4  43

#define ULTIMO_CHAR   '\n'

int distancias[RANGO];

const int trig  = 32;
const int echo  = 33;

AF_DCMotor motor1( 1 );
AF_DCMotor motor2( 2 );
AF_DCMotor motor3( 3 );
AF_DCMotor motor4( 4 );

void setup ()
{
  Serial.begin( 9600 );
  Serial1.begin( 9600 );
  
  for ( int i = 0; i < RANGO; i++ )
    distancias[i] = 0;

  pinMode( trig, OUTPUT );
  pinMode( echo, INPUT );
  digitalWrite( trig, LOW );

  pinMode( 42, INPUT );

  pinMode( MANUAL_X, INPUT );
  pinMode( MANUAL_Y, INPUT );

  pinMode( B1, INPUT );
  pinMode( B2, INPUT );
  pinMode( B3, INPUT );
  pinMode( B4, INPUT );

  pinMode( PIN_LED, OUTPUT );

  s.attach( PIN_SERVO );

  antes = 0;
}

bool calmate = false;
bool estado_automatico = false;

void enojado ()
{
  s.write( ENOJADO );
  digitalWrite( PIN_LED, HIGH );
}

void tranqui ()
{
  s.write( TRANQUI );
  digitalWrite( PIN_LED, LOW );
}

long t = 0;

int estado = 0;
int vel = 200;

bool borde ()
{
  return digitalRead( B1 ) == HIGH || digitalRead( B2 ) == HIGH || digitalRead( B3 ) == HIGH || digitalRead( B4 ) == HIGH;
}

bool encontrado ()
{
  return distancia() != -1;
}

void loop ()
{
  controlRemoto();

  if ( estado_automatico )
  {
    long ahora = millis();
    enojo -= ahora - antes;
    antes = ahora;
  
    int r = analogRead( PIN_MIC );
    Serial.println( r );
    
    if ( r > RUIDO )
    {
      enojo = T_ENOJO;
    }
  
    if ( enojo > 0 )
    {
      calmate = true;
      enojado();
    }
    else
    {
      tranqui();
  
      if ( calmate )
      {
        calmate = false;
        delay( 1000 );
      }
    }
  }
}

String comando = "";
void controlRemoto ()
{
  if ( Serial1.available() )
  {
    char c = Serial1.read();

    if ( c == ULTIMO_CHAR )
    {
      Serial.println( comando );
      procesarComando();
      comando = "";
    }
    else
      comando += c;
  }
}

void procesarComando ()
{
  String opcion = getFirst( comando );
  String parametros = delFirst( comando );
  int p;
  parametros > p;

  if ( opcion == "adelante" )
    adelante( p );
  else if ( opcion == "atras" )
    atras( p );
  else if ( opcion == "izquierda" )
    izquierda( p );
  else if ( opcion == "derecha" )
    derecha( p );

  else if ( opcion == "adelanteDer" )
    adelanteDer( p );
  else if ( opcion == "adelanteIzq" )
    adelanteIzq( p );
  else if ( opcion == "atrasDer" )
    atrasDer( p );
  else if ( opcion == "atrasIzq" )
    atrasIzq( p );

  else if ( opcion == "girarDer" )
    girarDer( p );
  else if ( opcion == "girarIzq" )
    girarIzq( p );
  
  else if ( opcion == "parar" )
    parar();

  else if ( opcion == "bender" )
  {    
    if ( p == 0 )
    {
      tranqui();
      estado_automatico = false;
    }
    else if ( p == 1 )
    {
      enojado();
      estado_automatico = false;
    }
    else if ( p == 2 )
    {
      estado_automatico = true;
    }
  }
}

void automatico ()
{
  if ( estado == 0 ) // Inicio
  {
    while ( !borde )
      adelante( vel );

    parar();
    estado = 1;
  }
  else if ( estado == 1 ) // Detecto borde
  {
    atras();
    delay( 500 );
    parar();

    estado = 2;
  }
  else if ( estado == 2 ) // Encontrar oponente
  {
    bool girDer = true;
    while ( !encontrado() )
    {
      if ( girDer )
        girarDer( vel );
      else
        girarIzq( vel );
      
      delay( 2000 );
      girDer = !girDer;
    }
    parar();

    estado = 3;
  }
  else if ( estado == 3 ) // Atacar
  {
    while ( !borde() )
      adelante( vel );

    estado = 1;
  }
}

int v ( int valor )
{
  return ( map( valor, 0, 1024, 0, 3 ) - 1 ) * -1;
}

void controlarManual ()
{
  int x = v( analogRead( MANUAL_X ) );
  int y = v( analogRead( MANUAL_Y ) );
  
  Serial.print( x );
  Serial.print( ", " );
  Serial.print( y );
  Serial.print( " - " );

  int d = dir( x, y );

  switch ( d )
  {
    case 0:
      Serial.println( "quieto" );
      parar();
      break;
    
    case 1:
      Serial.println( "adelante" );
      adelante( 255 );
      break;
    case 2:
      Serial.println( "atras" );
      atras( 255 );
      break;
    case 3:
      Serial.println( "derecha" );
      derecha( 255 );
      break;
    case 4:
      Serial.println( "izquierda" );
      izquierda( 255 );
      break;
    
    case 5:
      Serial.println( "adelante derecha" );
      adelanteDer( 255 );
      break;
    case 6:
      Serial.println( "adelante izquierda" );
      adelanteIzq( 255 );
      break;
    case 7:
      Serial.println( "atras derecha" );
      atrasDer( 255 );
      break;
    case 8:
      Serial.println( "atras izquierda" );
      atrasIzq( 255 );
      break;
  }
}

int dir ( int x, int y )
{
  if ( x == 0 && y == 0 )
    return 0;
  
  if ( x == 1 && y == 0 )
    return 1;
  if ( x == -1 && y == 0 )
    return 2;
  if ( x == 0 && y == 1 )
    return 3;
  if ( x == 0 && y == -1 )
    return 4;

  if ( x == 1 && y == 1 )
    return 5;
  if ( x == 1 && y == -1 )
    return 6;
  if ( x == -1 && y == 1 )
    return 7;
  if ( x == -1 && y == -1 )
    return 8;
}
