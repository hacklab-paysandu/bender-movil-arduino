long distancia ()
{
  digitalWrite( trig, LOW );
  delayMicroseconds( 5 );
  digitalWrite( trig, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( trig, LOW );

  long duracion = pulseIn( echo, HIGH, 30000 );
  long d = ( duracion / 2 ) / 29.1;

  if ( d < 5 || d > 50 )
    return -1;
  else
    return d;
}
/*
void sumar ( int v[], int n )
{
  for ( int i = RANGO - 1; i > 0; i-- )
    v[i] = v[i - 1];

  v[0] = n;
}

int promedio ( int v[] )
{
  int cant = 0;
  int suma = 0;
  
  for ( int i = 0; i < RANGO; i++ )
  {
    if ( v[i] != 0 )
    {
      suma += v[i];
      cant++;
    }
  }

  if ( cant == 0 )
    return 0;
  
  return suma / cant;
}*/
