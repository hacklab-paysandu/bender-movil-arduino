// https://www.youtube.com/watch?v=83tVkgT89dM&t=159s

// Distribucion de motores:
// M1 |-------| M4
//    |   ^   |
//    |   |   |
// M2 |-------| M3

void velocidad ( uint8_t vel )
{
  motor1.setSpeed( vel );
  motor2.setSpeed( vel );
  motor3.setSpeed( vel );
  motor4.setSpeed( vel );
}

// ------------------------------------ adelante
void adelante ( uint8_t vel )
{
  velocidad( vel );
  adelante();
}

void adelante ()
{
  motor1.run( FORWARD );
  motor2.run( FORWARD );
  motor3.run( FORWARD );
  motor4.run( FORWARD );
}

// ------------------------------------ atras
void atras ( uint8_t vel )
{
  velocidad( vel );
  atras();
}

void atras ()
{
  motor1.run( BACKWARD );
  motor2.run( BACKWARD );
  motor3.run( BACKWARD );
  motor4.run( BACKWARD );
}

// ------------------------------------ izquierda
void izquierda ( uint8_t vel )
{
  velocidad( vel );
  izquierda();
}

void izquierda ()
{
  motor1.run( BACKWARD );
  motor2.run( FORWARD );
  motor3.run( BACKWARD );
  motor4.run( FORWARD );
}

// ------------------------------------ derecha
void derecha ( uint8_t vel )
{
  velocidad( vel );
  derecha();
}

void derecha ()
{
  motor1.run( FORWARD );
  motor2.run( BACKWARD );
  motor3.run( FORWARD );
  motor4.run( BACKWARD );
}

// ------------------------------------ adelanteIzq
void adelanteIzq ( uint8_t vel )
{
  velocidad( vel );
  adelanteIzq();
}

void adelanteIzq ()
{
  motor1.run( RELEASE );
  motor2.run( FORWARD );
  motor3.run( RELEASE );
  motor4.run( FORWARD );
}

// ------------------------------------ adelanteDer
void adelanteDer ( uint8_t vel )
{
  velocidad( vel );
  adelanteDer();
}

void adelanteDer ()
{
  motor1.run( FORWARD );
  motor2.run( RELEASE );
  motor3.run( FORWARD );
  motor4.run( RELEASE );
}

// ------------------------------------ atrasIzq
void atrasIzq ( uint8_t vel )
{
  velocidad( vel );
  atrasIzq();
}

void atrasIzq ()
{
  motor1.run( BACKWARD );
  motor2.run( RELEASE );
  motor3.run( BACKWARD );
  motor4.run( RELEASE );
}

// ------------------------------------ atrasDer
void atrasDer ( uint8_t vel )
{
  velocidad( vel );
  atrasDer();
}

void atrasDer ()
{
  motor1.run( RELEASE );
  motor2.run( BACKWARD );
  motor3.run( RELEASE );
  motor4.run( BACKWARD );
}

// ------------------------------------ girarDer
void girarDer ( uint8_t vel )
{
  velocidad( vel );
  girarDer();
}

void girarDer ()
{
  motor1.run( FORWARD );
  motor2.run( FORWARD );
  motor3.run( BACKWARD );
  motor4.run( BACKWARD );
}

// ------------------------------------ girarIzq
void girarIzq ( uint8_t vel )
{
  velocidad( vel );
  girarIzq();
}

void girarIzq ()
{
  motor1.run( BACKWARD );
  motor2.run( BACKWARD );
  motor3.run( FORWARD );
  motor4.run( FORWARD );
}

// ------------------------------------ parar
void parar ()
{
  motor1.run( BRAKE );
  motor2.run( BRAKE );
  motor3.run( BRAKE );
  motor4.run( BRAKE );
  
  velocidad( 0 );
}
